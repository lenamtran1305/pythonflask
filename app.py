import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

#  connect to database information
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:123456@172.17.0.2:3306/mydb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))


# get all queries for product
# Use app.app_context() to set up the application context
def get_all_products():
    with app.app_context():
        db.create_all()
        products = Product.query.all()
        print('--- Product list ---')
        for p in products:
            print(p.id, p.name)
        print(' ---***--- ')


def get_one_product(id):
    with app.app_context():
        db.create_all()
        with db.session.begin():
            p = db.session.get(Product, id)
            print(p.id, p.name)


def create_product(name):
    product = Product(name=name)
    with app.app_context():
        db.create_all()
        db.session.add(product)
        db.session.commit()


def remove_product(id):
    with app.app_context():
        db.create_all()
        product = Product.query.get(id)
        if product:
            name = product.name
            db.session.delete(product)
            db.session.commit()
            print(f"Product '{name}' with ID {id} removed.")
        else:
            print(f"No product found with ID {id}.")


def update_product(id):
    with app.app_context():
        db.create_all()
        with db.session.begin():
            product = db.session.get(Product, id)
            if product:
                old_name = product.name
                print(f"Current name: {old_name}")
                new_name = input("Enter new product name: ")
                product.name = new_name
                db.session.commit()
                print(f"Product with ID {id} updated from '{old_name}' to '{new_name}'.")
            else:
                print(f"No product found with ID {id}.")


def print_menu():
    print("1. View all products")
    print("2. View one product")
    print("3. Create a new product")
    print("4. Remove a product")
    print("5. Update a product")
    print("6. Exit")


while True:
    print_menu()
    choice = input("Enter your choice: ")
    if choice == "1":
        get_all_products()
    elif choice == "2":
        id = input("Enter product ID: ")
        get_one_product(id)
    elif choice == "3":
        name = input("Enter product name: ")
        create_product(name)
    elif choice == "4":
        id = input("Enter product ID: ")
        remove_product(id)
    elif choice == "5":
        id = input("Enter product ID: ")
        update_product(id)
    elif choice == "6":
        break
    else:
        print("Invalid choice. Please try again.")
